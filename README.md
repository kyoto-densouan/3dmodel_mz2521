# README #

1/3スケールのSHARP SuperMZ MZ-2521風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- シャープ

## 発売時期
- 1985年10月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/MZ-2500)
- [「僧兵ちまちま」のゲーム日記。](https://blog.goo.ne.jp/timatima-psu/e/14f1ccd5c6e745a9c027af0959cc1511)
- [懐かしのパソコン](https://greendeepforest.com/?p=895)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_mz2521/raw/df7161ce3d21ca83b5223185558acc9cdd592ff7/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mz2521/raw/df7161ce3d21ca83b5223185558acc9cdd592ff7/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mz2521/raw/df7161ce3d21ca83b5223185558acc9cdd592ff7/ExampleImage.png)
